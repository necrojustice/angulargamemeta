'use strict';

/**
 * @ngdoc overview
 * @name gamefrontendApp
 * @description
 * # gamefrontendApp
 *
 * Main module of the application.
 */
angular
  .module('astrocriticApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'infinite-scroll',
    'apiMock'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/game/:gameId', {
        templateUrl: 'views/game.html',
        controller: 'GameCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(function(apiMockProvider) {
    apiMockProvider.config({
      mockDataPath: 'mock_data',
      apiPath: 'api'
    });
  });
