'use strict';

/**
 * @ngdoc function
 * @name astrocriticApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the astrocriticApp
 */
angular.module('astrocriticApp')
  .controller('MainCtrl', ['$scope', '$http', function($scope, $http){
  	var games = null;
  	$scope.games = [];
  	$scope.isBusy = false;

 	$scope.getGames = function(){
	    if($scope.isBusy === true){
	    	return; // request in progress, return
	    }

	    $scope.isBusy = true;

	    $http({method: 'GET', url: 'api/games', apiMock: true}).
	        success(function(data, status, headers, config){
	          if(undefined !== data){
	            var foundItems = angular.fromJson(data);
	            games = $scope.games;

	            if(games.length >0){
	             $scope.currentPage ++;
	            }

	            angular.forEach(foundItems, function(foundItem){
	              games.push(foundItem);
	            });
	          }
	          
	          $scope.isBusy = false;
	        }).
	        error(function(data, status, headers, config){
	          $scope.successMessage = null;
	          $scope.errorMessage = 'Error getting games.';
	        });
  	};
}]);