'use strict';

/**
 * @ngdoc function
 * @name astrocriticApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the astrocriticApp
 */
angular.module('astrocriticApp')
  .controller('GameCtrl', ['$scope', '$http', '$routeParams',  function($scope, $http, $routeParams){
  	$scope.gameId = $routeParams.gameId;
  	$scope.game = null;

  	$scope.$watch('gameId', function() {
	     $scope.fetchGame();
	});

 	$scope.fetchGame = function(){
	    $http({method: 'GET', url: "api/games", apiMock: true}).
	        success(function(data, status, headers, config){
	          if(undefined !== data){
	            var games = angular.fromJson(data);
	            $scope.game = games[$scope.gameId -1];
	          }
	        }).
	        error(function(data, status, headers, config){
	          $scope.successMessage = null;
	          $scope.errorMessage = 'Error fetching game.';
	        });
  	};
}]);