'use strict';

/**
 * @ngdoc function
 * @name astrocriticApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the astrocriticApp
 */
angular.module('astrocriticApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
